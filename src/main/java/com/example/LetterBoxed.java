package com.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LetterBoxed {
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            printDirections();
            return;
        }

        Guesser guesser = new Guesser(args[0]);

        if (guesser.isGameStateInError()) {
            printDirections();
            return;
        }

        try (Stream<String> filestream = Files.lines(Path.of(args[1]))) {
            List<String> possibleWords = filestream
                    // remove 1 and 2 character words
                    .filter(Guesser::isGreaterThan2Characters)
                    // format the remaining words to match the Guesser
                    .map(Guesser::toLetterBoxedFormat)
                    // remove words that contain letters not on the board
                    .filter(guesser::onlyLetterBoxedLetters)
                    // remove words that contain adjacent letters on the same side of the board
                    .filter(guesser::followsLetterBoxedPattern)
                    .collect(Collectors.toList());

            // Find all two word combinations from the above words
            List<Guess> answers = Guesser.findAllTwoWordCombinations(possibleWords)
                    .stream()
                    // filter to just the combinations that use all the letters on the board
                    .filter(guesser::containsAllLetterBoxedLetters)
                    .collect(Collectors.toList());

            for (Guess answer : answers) {
                System.out.println(answer);
            }
        } catch (IOException e) {
            printDirections();
        }
    }

    public static void printDirections() {
        System.out.println("The application expects a letter box in the format ABC,DEF,GHI,JKL and a path to a file as inputs.  Example 'java com.example.LetterBoxed \"CRE,ABD,TWO,PKN\" resources/words.txt");
    }
}
