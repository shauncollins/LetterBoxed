package com.example;

/**
 * Store the guess information for the LetterBoxed game.
 */
public class Guess {
    public String word1;
    public String word2;

    public Guess(String word1, String word2) {
        this.word1 = word1;
        this.word2 = word2;
    }

    public String getWord1() {
        return word1;
    }

    public String getWord2() {
        return word2;
    }

    public String toString() {
        return word1 + " " + word2;
    }
}
