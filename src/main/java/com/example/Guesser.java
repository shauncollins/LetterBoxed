package com.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The LetterBoxed application logic.  It expects input to be in the format "ABC,DEF,GHI,JKL".  It then has methods that
 * follow the rules of the LetterBoxed game to arrive at a list of 2 word solutions.
 */
public class Guesser {

    private boolean errorState;
    private String input;
    private String validLetters;
    private Map<Character, String> letterToGroupMap;

    public Guesser(String input) {
        this.input = input;
        initializeGameState();
    }

    /**
     * Verify the input data is valid for LetterBoxed and store some helper attributes on the class.
     */
    private void initializeGameState() {
        // Store the list of letters used in the puzzle in lower case to help searching words for them later.
        validLetters = input.replaceAll(",", "").toLowerCase();

        // initialize the error state to false
        errorState = false;

        // This is a map of letter -> "side of the box"
        letterToGroupMap = new HashMap<>();
        int commaCount = 0;
        int lettersInGroup = 0;
        for (Character chr : input.toLowerCase().toCharArray()) {
            if (chr == ',') {
                // if this group didn't contain 3 letters, the input was invalid.
                if (lettersInGroup != 3) {
                    errorState = true;
                }

                // update for the next group
                commaCount++;
                lettersInGroup = 0;
            } else {
                // store the group number on the character.
                letterToGroupMap.put(chr, Integer.toString(commaCount));
                lettersInGroup++;
            }
        }

        // validate the end state of the board.
        if (commaCount != 3 || letterToGroupMap.size() != 12) {
            errorState = true;
        }
    }

    /**
     * Make sure the given word is at least 3 letters long.
     * @param word the current word in the dictionary
     * @return true if the word is at least 3 characters.
     */
    public static boolean isGreaterThan2Characters(String word) {
        return word.length() > 2;
    }

    /**
     * Format the word from the dictionary to match the format of the Guesser.
     * @param word the word from the dictionary
     * @return a formatted version of the word
     */
    public static String toLetterBoxedFormat(String word) {
        return word.trim().toLowerCase();
    }

    /**
     * Given a list of words that match the LetterBoxed pattern, find all two word combinations where the last character
     * of the first word matches the first character of the 2nd word.
     * @param words all the words that match the LetterBoxed pattern.
     * @return A list of two word combinations
     */
    public static List<Guess> findAllTwoWordCombinations(List<String> words) {
        List<Guess> guesses = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            for (int j = 0; j < words.size(); j++) {
                if (i == j) {
                    continue;
                }
                // if the last letter of word i is the same as the first letter of word j, add that combo
                String wordi = words.get(i);
                String wordj = words.get(j);
                if (wordi.charAt(wordi.length() - 1) == wordj.charAt(0)) {
                    guesses.add(new Guess(wordi, wordj));
                }
            }
        }
        return guesses;
    }

    /**
     * Returns true if the input did not create a valid game board.
     * @return true if the game state is not playable.
     */
    public boolean isGameStateInError() {
        return errorState;
    }

    /**
     * Does the current word only contain letters in the LetterBoxed board?
     * @param word the word from the dictionary
     * @return true if the current word only contains letters from the board.
     */
    public boolean onlyLetterBoxedLetters(String word) {
        return word.chars().allMatch(chr -> this.validLetters.indexOf(chr) > -1);
    }

    /**
     * Does the current word not contain adjacent letters on the same side of the LetterBoxed game board?
     * @param word the word from the dictionary
     * @return true if the word correctly follows the Letterboxed side rules.
     */
    public boolean followsLetterBoxedPattern(String word) {
        StringBuilder updatedWord = new StringBuilder();
        for (Character chr : word.toCharArray()) {
            updatedWord.append(letterToGroupMap.get(chr));
        }
        Character previousCharacter = null;
        for (Character chr : updatedWord.toString().toCharArray()) {
            if (chr.equals(previousCharacter)) {
                return false;
            }
            previousCharacter = chr;
        }
        return true;
    }

    /**
     * Does the current guess use all the letters in the LetterBoxed board?
     * @param guess the combination of words.
     * @return true if the current guess uses all the letters.
     */
    public boolean containsAllLetterBoxedLetters(Guess guess) {
        String guessLetters = guess.getWord1() + guess.getWord2();
        return this.validLetters.chars().allMatch(chr -> guessLetters.indexOf(chr) >= 0);
    }
}
