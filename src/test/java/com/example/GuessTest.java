package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuessTest {

    @Test
    void testGetWord1() {
        Guess g = new Guess("a", "b");
        assertEquals("a", g.getWord1());
    }

    @Test
    void testGetWord2() {
        Guess g = new Guess("a", "b");
        assertEquals("b", g.getWord2());
    }
}
