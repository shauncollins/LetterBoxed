package com.example;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GuesserTest {

    @Test
    void testErrorStateWithNoBoard() {
        Guesser guesser = new Guesser("");
        assertTrue(guesser.isGameStateInError());
    }

    @Test
    void testErrorStateWithNotEnoughSides() {
        Guesser guesser = new Guesser("ABC,DEF");
        assertTrue(guesser.isGameStateInError());
    }

    @Test
    void testErrorStateWithTooManySides() {
        Guesser guesser = new Guesser("ABC,DEF,GHI,JKL,MNO");
        assertTrue(guesser.isGameStateInError());
    }

    @Test
    void testErrorStateWithNotEnoughCharactersOnASide() {
        Guesser guesser = new Guesser("ABC,DEF,GHI,JK");
        assertTrue(guesser.isGameStateInError());
    }

    @Test
    void testIsGreaterThanTwoCharactersTrue() {
        assertTrue(Guesser.isGreaterThan2Characters("abc"));
    }

    @Test
    void testIsGreaterThanTwoCharactersFalse() {
        assertFalse(Guesser.isGreaterThan2Characters("ab"));
    }

    @Test
    void testToLetterBoxedFormat() {
        assertEquals("abcdef", Guesser.toLetterBoxedFormat("ABCDEF"));
    }

    @Test
    void testFindAllTwoWordCombinations() {
        List<String> words = new ArrayList<>();
        words.add("abc");
        words.add("cde");
        words.add("jkl");
        words.add("arc");
        // should not have a ccc ccc match.
        words.add("ccc");

        List<Guess> guesses = Guesser.findAllTwoWordCombinations(words);
        assertEquals(5, guesses.size());
        assertTrue(guesses.stream().anyMatch(g -> "abc".equals(g.getWord1()) && "cde".equals(g.getWord2())));
        assertTrue(guesses.stream().anyMatch(g -> "abc".equals(g.getWord1()) && "ccc".equals(g.getWord2())));
        assertTrue(guesses.stream().anyMatch(g -> "arc".equals(g.getWord1()) && "cde".equals(g.getWord2())));
        assertTrue(guesses.stream().anyMatch(g -> "arc".equals(g.getWord1()) && "ccc".equals(g.getWord2())));
        assertTrue(guesses.stream().anyMatch(g -> "ccc".equals(g.getWord1()) && "cde".equals(g.getWord2())));
    }

    @Test
    void testOnlyLetterBoxLettersTrue() {
        Guesser guesser = new Guesser("ABC,DEF,GHI,JKL");
        assertTrue(guesser.onlyLetterBoxedLetters("abc"));
        assertTrue(guesser.onlyLetterBoxedLetters("agi"));
        assertTrue(guesser.onlyLetterBoxedLetters("abcdefghijkl"));
    }

    @Test
    void testOnlyLetterBoxLettersFalse() {
        Guesser guesser = new Guesser("ABC,DEF,GHI,JKL");
        assertFalse(guesser.onlyLetterBoxedLetters("mno"));
        assertFalse(guesser.onlyLetterBoxedLetters("abm"));
    }

    @Test
    void testFollowsLetterBoxPatternTrue() {
        Guesser guesser = new Guesser("ABC,DEF,GHI,JKL");
        assertTrue(guesser.followsLetterBoxedPattern("adgj"));
        assertTrue(guesser.followsLetterBoxedPattern("blge"));
        assertTrue(guesser.followsLetterBoxedPattern("adadad"));
    }

    @Test
    void testFollowsLetterBoxPatternFalse() {
        Guesser guesser = new Guesser("ABC,DEF,GHI,JKL");
        assertFalse(guesser.followsLetterBoxedPattern("aaaa"));
        assertFalse(guesser.followsLetterBoxedPattern("adgi"));
        assertFalse(guesser.followsLetterBoxedPattern("add"));
    }

    @Test
    void testContainsAllLetterBoxLettersTrue() {
        Guesser guesser = new Guesser("ABC,DEF,GHI,JKL");
        assertTrue(guesser.containsAllLetterBoxedLetters(new Guess("abcdef", "ghijkl")));
        assertTrue(guesser.containsAllLetterBoxedLetters(new Guess("abcdefghijk", "l")));
        assertTrue(guesser.containsAllLetterBoxedLetters(new Guess("bcfhila", "jegdk")));
        assertTrue(guesser.containsAllLetterBoxedLetters(new Guess("bcfdkhila", "cjegdlk")));
    }

    @Test
    void testContainsAllLetterBoxLettersFalse() {
        Guesser guesser = new Guesser("ABC,DEF,GHI,JKL");
        assertFalse(guesser.containsAllLetterBoxedLetters(new Guess("abc", "ghi")));
        assertFalse(guesser.containsAllLetterBoxedLetters(new Guess("abcdefgijk", "l")));
        assertFalse(guesser.containsAllLetterBoxedLetters(new Guess("bbcfibla", "jbegdbk")));
    }
}
